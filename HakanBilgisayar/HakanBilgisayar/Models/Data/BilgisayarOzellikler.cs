﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HakanBilgisayar.Models
{
    public class BilgisayarOzellikler
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "Bilgisayar Tipi :")]
        public string BilgisayarTipi { get; set; }
        public string Islemcisi { get; set; }
        public string RamBoyutu { get; set; }
        public string HardDiskBoyutu { get; set; }
        public string Agirligi { get; set; }
        public string EkranKartiBoyutu { get; set; }
        public string Cozunurlugu { get; set; }
    }
}