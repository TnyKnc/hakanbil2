namespace HakanBilgisayar.Migrations.HakanBilContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YaziciModelEkle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Yazici",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Marka = c.String(),
                        Model = c.String(),
                        Fiyat = c.Int(nullable: false),
                        Ozellikler = c.String(),
                        YaziciFoto = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Yazici");
        }
    }
}
