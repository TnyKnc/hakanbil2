﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HakanBilgisayar.DAL;
using HakanBilgisayar.Models.Data;
using HakanBilgisayar.Models;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace HakanBilgisayar.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private HakanBilContext db = new HakanBilContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();    
        }

        //#region // Slider
        // GET: Admin/Details/5

        public ActionResult Bilgisayar()
        {
            return View(db.bilgisayar.ToList());
        }
        public ActionResult BilgisayarDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bilgisayar bilgisayar = db.bilgisayar.Find(id);
            if (bilgisayar == null)
            {
                return HttpNotFound();
            }
            return View(bilgisayar);
        }

        // GET: Admin/Create
        public ActionResult BilgisayarEkle()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BilgisayarEkle(Bilgisayar b, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    Bilgisayar bilgisayar = new Bilgisayar();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        bilgisayar.BilgisayarFoto = memoryStream.ToArray();
                    }
                    bilgisayar.Fiyat = b.Fiyat;
                    bilgisayar.Marka = b.Marka;
                    bilgisayar.Model = b.Model;
                    bilgisayar.Ozellikler = b.Ozellikler;
                    bilgisayar.Tipi = b.Tipi;
                    
                    context.bilgisayar.Add(bilgisayar);
                    context.SaveChanges();
                    return RedirectToAction("Bilgisayar", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        // GET: Admin/Edit/5
        public ActionResult BilgisayarDuzenle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bilgisayar bilgisayar = db.bilgisayar.Find(id);
            if (bilgisayar == null)
            {
                return HttpNotFound();
            }
            return View(bilgisayar);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BilgisayarDuzenle(Bilgisayar b, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    var _bilgisayarDuzenle = context.bilgisayar.Where(x => x.ID == b.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _bilgisayarDuzenle.BilgisayarFoto = memoryStream.ToArray();
                    }
                    _bilgisayarDuzenle.Fiyat = b.Fiyat;
                    _bilgisayarDuzenle.Marka = b.Marka;
                    _bilgisayarDuzenle.Model = b.Model;
                    _bilgisayarDuzenle.Ozellikler = b.Ozellikler;
                    _bilgisayarDuzenle.Tipi = b.Tipi;
                    context.SaveChanges();
                    return RedirectToAction("Bilgisayar", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }

        // GET: Admin/Delete/5
        public ActionResult BilgisayarSil(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bilgisayar bilgisayar = db.bilgisayar.Find(id);
            if (bilgisayar == null)
            {
                return HttpNotFound();
            }
            return View(bilgisayar);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("BilgisayarSil")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bilgisayar bilgisayar = db.bilgisayar.Find(id);
            db.bilgisayar.Remove(bilgisayar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Slider()
        {
            using (HakanBilContext context = new HakanBilContext())
            {
                var slider = context.Slider.ToList();
                return View(slider);
            }
        }
        public ActionResult SlideEkle()
        {
            return View();
        }
        public ActionResult SlideDuzenle(int SlideID)
        {
            using (HakanBilContext context = new HakanBilContext())
            {
                var _slideDuzenle = context.Slider.Where(x => x.ID == SlideID).FirstOrDefault();
                return View(_slideDuzenle);
            }
        }
        public ActionResult SlideSil(int SlideID)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    context.Slider.Remove(context.Slider.First(d => d.ID == SlideID));
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult SlideEkle(Slider s, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    Slider _slide = new Slider();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slide.SliderFoto = memoryStream.ToArray();
                    }
                    _slide.SliderText = s.SliderText;
                    _slide.BaslangicTarih = s.BaslangicTarih;
                    _slide.BitisTarih = s.BitisTarih;
                    context.Slider.Add(_slide);
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(Slider slide, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    var _slideDuzenle = context.Slider.Where(x => x.ID == slide.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slideDuzenle.SliderFoto = memoryStream.ToArray();
                    }
                    _slideDuzenle.SliderText = slide.SliderText;
                    _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                    _slideDuzenle.BitisTarih = slide.BitisTarih;
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        public ActionResult Elektronik()
        {
            return View(db.elektronik.ToList());
        }
        public ActionResult ElektronikDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Elektronik elektronik = db.elektronik.Find(id);
            if (elektronik== null)
            {
                return HttpNotFound();
            }
            return View(elektronik);
        }

        // GET: Admin/Create
        public ActionResult ElektronikEkle()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ElektronikEkle(Elektronik e, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    Elektronik elektronik = new Elektronik();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        elektronik.ElektronikFoto = memoryStream.ToArray();
                    }
                    elektronik.Fiyat = e.Fiyat;
                    elektronik.Marka = e.Marka;
                    elektronik.Model = e.Model;
                    elektronik.Ozellikler = e.Ozellikler;

                    context.elektronik.Add(elektronik);
                    context.SaveChanges();
                    return RedirectToAction("Elektronik", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        // GET: Admin/Edit/5
        public ActionResult ElektronikDuzenle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Elektronik elektronik= db.elektronik.Find(id);
            if (elektronik == null)
            {
                return HttpNotFound();
            }
            return View(elektronik);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ElektronikDuzenle(Elektronik e, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    var _elektronikDuzenle = context.elektronik.Where(x => x.ID == e.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _elektronikDuzenle.ElektronikFoto = memoryStream.ToArray();
                    }
                    _elektronikDuzenle.Fiyat = e.Fiyat;
                    _elektronikDuzenle.Marka = e.Marka;
                    _elektronikDuzenle.Model = e.Model;
                    _elektronikDuzenle.Ozellikler = e.Ozellikler;
                    context.SaveChanges();
                    return RedirectToAction("Elektronik", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }

        // GET: Admin/Delete/5
        public ActionResult ElektronikSil(int? id)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    context.elektronik.Remove(context.elektronik.First(d => d.ID == id));
                    context.SaveChanges();
                    return RedirectToAction("Elektronik", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

      

        
        public ActionResult Yazici()
        {
            return View(db.yazici.ToList());
        }
        public ActionResult YaziciDetay(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazici yazici= db.yazici.Find(id);
            if (yazici== null)
            {
                return HttpNotFound();
            }
            return View(yazici);
        }

        // GET: Admin/Create
        public ActionResult YaziciEkle()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult YaziciEkle(Yazici y, HttpPostedFileBase file)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    Yazici yazici= new Yazici();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        yazici.YaziciFoto = memoryStream.ToArray();
                    }
                    yazici.Fiyat = y.Fiyat;
                    yazici.Marka = y.Marka;
                    yazici.Model = y.Model;
                    yazici.Ozellikler = y.Ozellikler;

                    context.yazici.Add(yazici);
                    context.SaveChanges();
                    return RedirectToAction("Yazici", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        // GET: Admin/Edit/5
        public ActionResult YaziciDuzenle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazici yazici= db.yazici.Find(id);
            if (yazici == null)
            {
                return HttpNotFound();
            }
            return View(yazici);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult YaziciDuzenle(Yazici y, HttpPostedFileBase file)
        //{
        //    try
        //    {
        //        using (HakanBilContext context = new HakanBilContext())
        //        {
        //            var _yaziciDuzenle = context.yazici.Where(x => x.ID == y.ID).FirstOrDefault();
        //            if (file != null && file.ContentLength > 0)
        //            {
        //                MemoryStream memoryStream = file.InputStream as MemoryStream;
        //                if (memoryStream == null)
        //                {
        //                    memoryStream = new MemoryStream();
        //                    file.InputStream.CopyTo(memoryStream);
        //                }
        //                _yaziciDuzenle.yaziciFoto = memoryStream.ToArray();
        //            }
        //            _yaziciDuzenle.Fiyat = y.Fiyat;
        //            _yaziciDuzenle.Marka = y.Marka;
        //            _yaziciDuzenle.Model = y.Model;
        //            _yaziciDuzenle.Ozellikler = y.Ozellikler;
        //            context.SaveChanges();
        //            return RedirectToAction("Yazici", "Admin");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Güncellerken hata oluştu " + ex.Message);
        //    }
        //}

        // GET: Admin/Delete/5
        public ActionResult YaziciSil(int? id)
        {
            try
            {
                using (HakanBilContext context = new HakanBilContext())
                {
                    context.yazici.Remove(context.yazici.First(d => d.ID == id));
                    context.SaveChanges();
                    return RedirectToAction("Yazici", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Rol()
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            return View(roleManager.Roles.ToList());
        }

        public ActionResult RolEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolEkle(RolModel rol)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (roleManager.RoleExists(rol.RolAdi) == false)
            {
                roleManager.Create(new IdentityRole(rol.RolAdi));
            }
            return RedirectToAction("Rol");
        }

        public ActionResult RolKullaniciEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RolKullaniciEkle(RolKullaniciEkleModel model)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.KullaniciAdi);

            userManager.AddToRole(kullanici.Id, model.RolAdi);

            return RedirectToAction("Rol");
        }


     
    }
}

