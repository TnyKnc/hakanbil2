﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HakanBilgisayar.Startup))]
namespace HakanBilgisayar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
