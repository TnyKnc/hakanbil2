namespace HakanBilgisayar.Migrations.HakanBilContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BilgisayarFotoEkle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bilgisayar", "BilgisayarFoto", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bilgisayar", "BilgisayarFoto");
        }
    }
}
