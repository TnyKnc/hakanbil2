﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HakanBilgisayar.Models.Data
{
    public class Elektronik
    {
            public int ID { get; set; }
            public string Marka { get; set; }
            public string Model { get; set; }
            public int Fiyat { get; set; }
            public string Ozellikler { get; set; }
            public byte[] ElektronikFoto { get; set; }

    }
}