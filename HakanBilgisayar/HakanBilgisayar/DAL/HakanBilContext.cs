﻿using HakanBilgisayar.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using HakanBilgisayar.Models.Data;

namespace HakanBilgisayar.DAL
{
    public class HakanBilContext : DbContext
    {
        public HakanBilContext():base("HakanBilContext")
        {
        }
        
        public DbSet<Bilgisayar> bilgisayar { get; set; }
        public DbSet<Elektronik> elektronik { get; set; }
        public DbSet<Yazici> yazici { get; set; }
        public DbSet<Slider> Slider { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
 	         modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}