namespace HakanBilgisayar.Migrations.HakanBilContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ElektronikModelEkle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Elektronik",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Marka = c.String(),
                        Model = c.String(),
                        Fiyat = c.Int(nullable: false),
                        Ozellikler = c.String(),
                        ElektronikFoto = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Elektronik");
        }
    }
}
