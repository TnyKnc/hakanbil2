namespace HakanBilgisayar.Migrations.HakanBilContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bilgisayar",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Marka = c.String(),
                        Model = c.String(),
                        Fiyat = c.Int(nullable: false),
                        Ozellikler = c.String(),
                        Tipi = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Slider",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SliderFoto = c.Binary(),
                        SliderText = c.String(),
                        BaslangicTarih = c.DateTime(),
                        BitisTarih = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Slider");
            DropTable("dbo.Bilgisayar");
        }
    }
}
