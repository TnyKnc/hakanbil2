﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HakanBilgisayar.DAL;
using HakanBilgisayar.Models;
using HakanBilgisayar.Models.Data;

namespace HakanBilgisayar.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? id)
        {
            using (HakanBilContext context = new HakanBilContext())
            {
                    AnaSayfaDTO anasayfa = new AnaSayfaDTO();
                    anasayfa.bilgisayar = context.bilgisayar.OrderByDescending(x => x.ID).ToList();
                    anasayfa.slider = context.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
                    return View(anasayfa);
            }
            
        }
        public ActionResult Elektronik()
        {
            using (HakanBilContext context = new HakanBilContext())
            {
            AnaSayfaDTO anasayfa = new AnaSayfaDTO();
            anasayfa.elektronik = context.elektronik.OrderByDescending(x => x.ID).ToList();
            anasayfa.slider = context.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
            return View(anasayfa);
            }
        }
        public ActionResult Yazici()
        {
            using (HakanBilContext context = new HakanBilContext())
            {
                AnaSayfaDTO anasayfa = new AnaSayfaDTO();
                anasayfa.yazici = context.yazici.OrderByDescending(x => x.ID).ToList();
                anasayfa.slider = context.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
                return View(anasayfa);
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Bilgisayar()
        {
            using (HakanBilContext context = new HakanBilContext())
            {
                AnaSayfaDTO anasayfa = new AnaSayfaDTO();
                anasayfa.bilgisayar = context.bilgisayar.OrderByDescending(x => x.ID).ToList();
                return View(anasayfa);
            }
        }
    }

    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Bilgisayar> bilgisayar { get; set; }
        public List<Elektronik> elektronik { get; set; }
        public List<Yazici> yazici { get; set; }
    }
}