﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HakanBilgisayar.Models
{
    public class RolKullaniciEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set;}
    }
}